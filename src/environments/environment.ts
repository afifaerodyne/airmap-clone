// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mapbox: {
    accessToken: 'pk.eyJ1IjoiYXp1ZGRpbi1hZXJvZHluZSIsImEiOiJja2ZuZzd2a3gwNzJvMnFxbzY4b2xqcmdkIn0.n9YHUYWsgqK1RuoRwmm_qw',
    defaultStyle: 'mapbox://styles/azuddin-aerodyne/ckfnl4nvx0aed19qu3techbeg'
  },
  openWeather: {
    apiKey: '93314db9b5a05cd94a4737e72f295a3b'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
