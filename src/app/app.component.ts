import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements AfterViewInit {
  title = 'airmap-clone';

  @ViewChild('tnc')
  public readonly tnc!: SwalComponent;

  ngAfterViewInit(): void {
    // check if user has accept tnc
    // this.tnc.fire()
  }

  acceptTnc(): void {
    console.log('accept tnc')
  }
}
