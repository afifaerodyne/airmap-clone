import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { HomeModule } from './modules/home/home.module';
import { SupportModule } from './modules/support/support.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

const config = {
    databaseURL: 'https://airmap-clone-default-rtdb.firebaseio.com/',

    apiKey: 'AIzaSyAW9u4QU_Semgxh2xjDadLT8e29gUo1_9w',
    authDomain: 'airmap-clone.firebaseapp.com',
    projectId: 'airmap-clone',
    storageBucket: 'airmap-clone.appspot.com',
    messagingSenderId: '1014448191496',
    appId: '1:1014448191496:web:62859df611174d1310b9bd'
};
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,

    AngularFireModule.initializeApp(config),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage

    HomeModule,
    SupportModule,

    NgxSpinnerModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
