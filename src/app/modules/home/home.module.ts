import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import {
  FlightsComponent,
  FlightsListComponent,
} from './pages/flights/flights.component';
import {
  AircraftsComponent,
  AircraftsListComponent,
} from './pages/aircrafts/aircrafts.component';
import {
  ProfileComponent,
  ProfileContentComponent,
} from './pages/profile/profile.component';
import {
  SettingsComponent,
  SettingsContentComponent,
} from './pages/settings/settings.component';
import { MapModule } from 'src/app/shared/components/map/map.module';
import { FabButtonModule } from 'src/app/shared/components/fab-button/fab-button.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SmallWeatherWidgetModule } from 'src/app/shared/components/small-weather-widget/small-weather-widget.module';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { AgGridModule } from 'ag-grid-angular';
import { FightsFormComponent } from './components/fights-form/fights-form.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'flights',
        component: FlightsComponent,
      },
      {
        path: 'aircrafts',
        component: AircraftsComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'settings',
        component: SettingsComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    HomeComponent,
    FlightsComponent,
    AircraftsComponent,
    ProfileComponent,
    SettingsComponent,
    FlightsListComponent,
    AircraftsListComponent,
    ProfileContentComponent,
    SettingsContentComponent,
    FightsFormComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MapModule,
    FabButtonModule,
    FormsModule,
    ReactiveFormsModule,
    SmallWeatherWidgetModule,

    MatButtonModule,
    MatDialogModule,

    AgGridModule,
  ],
})
export class HomeModule {}
