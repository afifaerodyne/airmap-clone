import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aircrafts',
  template: '',
  styleUrls: ['./aircrafts.component.sass']
})
export class AircraftsComponent implements OnInit {

  constructor(private dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    const dialogRef = this.dialog.open(AircraftsListComponent);
    dialogRef.afterClosed().subscribe(res => this.router.navigate(['/']));
  }

}

@Component({
  selector: 'app-flight-content',
  templateUrl: './aircrafts.component.html',
  styleUrls: ['./aircrafts.component.sass']
})
export class AircraftsListComponent implements OnInit {
  columnDefs = [
    { field: 'make' },
    { field: 'model' },
    { field: 'price'}
  ];

  rowData = [
      { make: 'Toyota', model: 'Celica', price: 35000 },
      { make: 'Ford', model: 'Mondeo', price: 32000 },
      { make: 'Porsche', model: 'Boxter', price: 72000 }
  ];

  constructor() {}

  ngOnInit(): void {}

  onGridReady(params): void {
    params.api.sizeColumnsToFit();
  }
}
