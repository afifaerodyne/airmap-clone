import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FAB } from 'src/app/shared/components/fab-button/fab-button.component';
import { Subject } from 'rxjs';
import { SettingStoreService } from 'src/app/core/stores/setting-store.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
  animations: [
    trigger('sidebar', [
      transition(':enter', [
        style({ width: 0, opacity: 0 }),
        animate('.5s ease-out', style({ width: '25vw', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ width: '25vw', opacity: 1 }),
        animate('.5s ease-in', style({ width: 0, opacity: 0 })),
      ]),
    ]),
    trigger('flightForm', [
      transition(':enter', [
        style({ width: 0, opacity: 0 }),
        animate('.5s ease-out', style({ width: '40vw', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ width: '40vw', opacity: 1 }),
        animate('.5s ease-in', style({ width: 0, opacity: 0 })),
      ]),
    ]),
    trigger('mobileFlightForm', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('.3s ease-out', style({ height: '40vh', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ height: '40vh', opacity: 1 }),
        animate('.3s ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
    trigger('mobileMenu', [
      transition(':enter', [
        style({ maxHeight: 0, opacity: 0 }),
        animate('.05s ease-out', style({ maxHeight: '50vh', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ maxHeight: '50vh', opacity: 1 }),
        animate('.05s ease-in', style({ maxHeight: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class HomeComponent implements OnInit {

  allDrawing = [];
  buildMap = false;
  currentCoordinate: number[] = [0, 0];
  easeTo: any;
  extrusionOpt;
  extrusionHeight = 10;
  extrusionRadius = 10;
  fabButtons: FAB[] = [
    {
      id: 'add',
      color: '#007bff',
      icon: 'fa-plus',
    },
  ];
  isDropdownShow = false;
  isFormShow = false;
  isMobileFlightFormOpen = false;
  mapCentroidCoordinate: number[] = [0, 0];
  mapStyle: string;
  smallWeatherWidgetCoordinate: number[] = [0, 0];
  unsubber: Subject<boolean> = new Subject<boolean>();

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private settingStore: SettingStoreService
  ) {}

  ngOnInit(): void {
    this.spinner.show();

    this.extrusionOpt = {
      height: this.extrusionHeight,
      radius: this.extrusionRadius,
    };

    this.settingStore.setting$
      .pipe(takeUntil(this.unsubber.asObservable()))
      .subscribe(res => {
        this.mapStyle = res.map_style;
      });
  }

  onToggleDropdown(stat?: boolean): void {
    this.isDropdownShow = stat !== undefined ? stat : !this.isDropdownShow;
  }

  onClickDropdownList(fn: string, route: string): void {
    this.isDropdownShow = false;

    switch (fn) {
      case 'logout':
        this.router.navigate(['/']);
        break;

      default:
        break;
    }
    if (route !== undefined) {
      this.router.navigate([route]);
    }
  }

  onFabClick(e): void {
    this.isFormShow = !this.isFormShow;
  }

  onMapLoaded(stat): void {
    if (stat) {
      this.spinner.hide();
    }
  }

  onDrawUpdated(drawing): void {
    this.allDrawing.push(drawing);
  }

  onDrawDeleted(deletedDraw): void {
    this.allDrawing = this.allDrawing.filter(
      (drawing) => drawing.id !== deletedDraw.id
    );
  }

  onAltitudeChange(value): void {
    this.extrusionOpt = {
      ...this.extrusionOpt,
      height: value,
    };
  }

  onRadiusChange(value): void {
    this.extrusionOpt = {
      ...this.extrusionOpt,
      radius: value,
    };
  }

  onCurrentCoordinate($event): void {
    this.currentCoordinate = $event;
    this.smallWeatherWidgetCoordinate = $event;
  }

  onVisibleCentroidChange($event): void {
    this.smallWeatherWidgetCoordinate = $event;
  }

  onSidebarAnimationDone($event): void {
    this.buildMap = true;
  }

  onAnimationDone($event): void {
    const { toState, element } = $event;
    const display = window.getComputedStyle(element).display;

    if (toState === 'void') {
      this.easeTo = { bottom: 0, right: 0 };
    } else {
      switch ($event.triggerName) {
        case 'flightForm':
          if (display === 'flex') { this.easeTo = { right: document.documentElement.clientWidth * 0.1 }; }
          break;

        case 'mobileFlightForm':
          if (display === 'flex') { this.easeTo = { bottom: document.documentElement.clientHeight * 0.3 }; }
          break;

        default:
          break;
      }
    }

  }
}