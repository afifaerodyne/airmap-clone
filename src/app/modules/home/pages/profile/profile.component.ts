import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  template: '',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  constructor(private dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    const dialogRef = this.dialog.open(ProfileContentComponent)
    dialogRef.afterClosed().subscribe(res => this.router.navigate(['/']))
  }

}

@Component({
  selector: 'app-profile-content',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileContentComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {}
}