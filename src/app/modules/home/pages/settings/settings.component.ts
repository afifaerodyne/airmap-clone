import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SettingStoreService } from 'src/app/core/stores/setting-store.service';

@Component({
  selector: 'app-settings',
  template: '',
  styleUrls: ['./settings.component.sass']
})
export class SettingsComponent implements OnInit {

  constructor(private dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    const dialogRef = this.dialog.open(SettingsContentComponent);
    dialogRef.afterClosed().subscribe(res => this.router.navigate(['/']));
  }

}

@Component({
  selector: 'app-setting-content',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.sass']
})
export class SettingsContentComponent implements OnInit {
  generalSettingForm: FormGroup;
  unsubber: Subject<boolean> = new Subject<boolean>();

  constructor(private formBuilder: FormBuilder, private settingStore: SettingStoreService) {}

  ngOnInit(): void {
    const settingStore = this.settingStore.setting$.getValue();
    this.generalSettingForm = this.formBuilder.group({
      map_style: [settingStore.map_style ?? 'light'],
      flight_is_public: [settingStore.flight_is_public ?? false]
    });

    this.onFormChange();
  }

  onFormChange(): void {
    this.generalSettingForm.valueChanges.pipe(takeUntil(this.unsubber.asObservable())).subscribe(chg => {
      this.settingStore.setting$.next({map_style: chg.map_style});
    });
  }
}
