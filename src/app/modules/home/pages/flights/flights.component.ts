import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-flights',
  template: '',
  styleUrls: ['./flights.component.sass']
})
export class FlightsComponent implements OnInit {

  constructor(private dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    const dialogRef = this.dialog.open(FlightsListComponent);
    dialogRef.afterClosed().subscribe(res => this.router.navigate(['/']));
  }

}

@Component({
  selector: 'app-flight-content',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.sass']
})
export class FlightsListComponent implements OnInit {

  columnDefs = [
    { field: 'make' },
    { field: 'model' },
    { field: 'price'}
  ];

  rowData = [
      { make: 'Toyota', model: 'Celica', price: 35000 },
      { make: 'Ford', model: 'Mondeo', price: 32000 },
      { make: 'Porsche', model: 'Boxter', price: 72000 }
  ];

  constructor() {}

  ngOnInit(): void {}

  onGridReady(params): void {
    params.api.sizeColumnsToFit();
  }
}