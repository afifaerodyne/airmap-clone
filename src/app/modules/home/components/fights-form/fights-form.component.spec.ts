import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FightsFormComponent } from './fights-form.component';

describe('FightsFormComponent', () => {
  let component: FightsFormComponent;
  let fixture: ComponentFixture<FightsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FightsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FightsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
