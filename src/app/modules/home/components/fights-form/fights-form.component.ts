import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-fights-form',
  templateUrl: './fights-form.component.html',
  styleUrls: ['./fights-form.component.sass']
})
export class FightsFormComponent implements OnInit {
  @Output() altitude: EventEmitter<any>  = new EventEmitter();
  @Output() radius: EventEmitter<any>  = new EventEmitter();
  flightForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.flightForm = this.formBuilder.group({
      flightRadius: [10, Validators.required],
      flightAltitude: [10, Validators.required],
      dateTime: [''],
      duration: [60, Validators.required],
      pilot: [1, Validators.required],
      aircraft: [2, Validators.required],
      vlos: ['1', Validators.required],
      overPeople: ['1', Validators.required],
      aircraftWeight: [10, Validators.required],
    });
  }

  onAltitudeChange($event): void {
    this.altitude.emit(this.flightForm.get('flightAltitude').value);
  }

  onRadiusChange($event): void {
    this.radius.emit(this.flightForm.get('flightRadius').value);
  }

  minuteToHourMinute(n): string {
    const num = n;
    const hours = num / 60;
    const rhours = Math.floor(hours);
    const minutes = (hours - rhours) * 60;
    const rminutes = Math.round(minutes);

    return rhours + ' hour(s) and ' + rminutes + ' minute(s)';
  }
}
