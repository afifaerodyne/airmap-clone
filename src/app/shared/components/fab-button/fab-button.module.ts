import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FabButtonComponent } from './fab-button.component';

@NgModule({
  declarations: [FabButtonComponent],
  imports: [
    CommonModule
  ],
  exports: [FabButtonComponent]
})
export class FabButtonModule { }
