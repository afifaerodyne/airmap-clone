import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-fab-button',
  templateUrl: './fab-button.component.html',
  styleUrls: ['./fab-button.component.sass']
})
export class FabButtonComponent implements OnInit {
  isFabActive = false;
  @Input() buttons: FAB[];
  @Output() fabClicked: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  toggleFab(): void {
    this.isFabActive = !this.isFabActive;
  }

  onClick(id: string): void {
    this.fabClicked.emit(id);
  }
}

export interface FAB {
  id: string;
  color: string;
  icon: string;
}
