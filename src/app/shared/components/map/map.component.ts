import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import MapboxDraw from '@mapbox/mapbox-gl-draw';
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import { environment } from 'src/environments/environment';
import buffer from '@turf/buffer';

const DRAW_START_TEXT = 'Click drawing tool on the right to start drawing!';
const DRAW_LINE_STRING_TEXT = 'Click on map to draw the flight path. Then click again the last point to close it.';
const DRAW_POLYGON_TEXT = 'Click on map to draw the flight area. Then click again the first point to close it.';
const DRAW_POINT_TEXT = 'Click on map to drop flight area point.';
const DRAW_MORE_TEXT = 'Click drawing tool on the right to draw another.';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass'],
})
export class MapComponent implements OnInit, OnChanges {
  @Input() currLocMarkerCoor: string[];
  @Input() drawings: any[];
  @Input() easeTo: any;
  @Input() initExtrusionBaseHeight = 0;
  @Input() initExtrusionColor = '#3bb2d0';
  @Input() initExtrusionHeight = 10;
  @Input() initExtrusionOpacity = 0.5;
  @Input() initExtrusionRadius = 10;
  @Input() initExtrusionOpt: any = {
    color: this.initExtrusionColor,
    height: this.initExtrusionHeight,
    baseHeight: this.initExtrusionBaseHeight,
    opacity: this.initExtrusionOpacity,
    radius: this.initExtrusionRadius,
  };
  @Input() mapStyle = environment.mapbox.defaultStyle;
  @Input() toggleDrawMode: boolean;
  @Input() triggerBuildMap: boolean;
  @Input() markerSize = 100;

  @Output() mapLoaded: EventEmitter<boolean> = new EventEmitter();
  @Output() drawUpdated: EventEmitter<any> = new EventEmitter();
  @Output() drawDeleted: EventEmitter<any> = new EventEmitter();
  @Output() currentCoordinate: EventEmitter<number[]> = new EventEmitter();
  @Output() visibleMapCentroid: EventEmitter<number[]> = new EventEmitter();

  map: mapboxgl.Map;
  draw: MapboxDraw;
  drawInfo: DrawInfoHelper;
  geocoder: MapboxGeocoder;
  currCoor: number[] = [0, 0];
  extrusionOpt: any = {
    color: this.initExtrusionColor,
    height: this.initExtrusionHeight,
    baseHeight: this.initExtrusionBaseHeight,
    opacity: this.initExtrusionOpacity,
    radius: this.initExtrusionRadius,
  };

  constructor() {
    mapboxgl.accessToken = environment.mapbox.accessToken;
  }

  ngOnInit(): void {
    /**
     * Build map when receiving input buildMap === true
     */
    this.extrusionOpt = {
      color: this.initExtrusionColor,
      height: this.initExtrusionHeight,
      baseHeight: this.initExtrusionBaseHeight,
      opacity: this.initExtrusionOpacity,
      radius: this.initExtrusionRadius,
      ...this.initExtrusionOpt
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { drawings, easeTo, initExtrusionOpt, mapStyle, toggleDrawMode, triggerBuildMap } = changes;

    if (drawings && drawings.currentValue.length === 0) {
      const helperText = document.querySelector(
        '#drawInfoHelper > p'
      );
      if (helperText) {helperText.innerHTML = DRAW_START_TEXT; }
    }

    if (easeTo && this.map) {
      this.map.easeTo({
        padding: easeTo.currentValue,
        duration: 1000,
      });
    }

    if (initExtrusionOpt) {
      this.extrusionOpt = {...this.extrusionOpt, ...initExtrusionOpt.currentValue};
      const { radius } = initExtrusionOpt.currentValue;
      this.drawings.forEach((drawing) => {
        if (
          drawing.geometry.type === 'LineString' ||
          drawing.geometry.type === 'Point'
        ) {
          drawing = {
            ...buffer(drawing.geometry, radius / 1000),
            id: drawing.id,
          };
        }
        this.generateExtrusionLayer(this.map, drawing);
      });
    }

    if (mapStyle) {
      if (mapStyle.currentValue === null || mapStyle.currentValue === undefined) {return; }
      let style;
      switch (mapStyle.currentValue) {
        case 'satellite':
          style = 'mapbox://styles/mapbox/satellite-v9';
          break;
        case 'dark':
          style = 'mapbox://styles/mapbox/dark-v10';
          break;
        case 'street':
          style = 'mapbox://styles/mapbox/streets-v11';
          break;
        case 'outdoor':
          style = 'mapbox://styles/mapbox/outdoors-v11';
          break;
        default:
          style = environment.mapbox.defaultStyle;
          break;
      }
      this.map?.setStyle(style);
      this.map?.on('style.load', () => this.addCurrLocMarker());
    }

    if (toggleDrawMode) {
      switch (toggleDrawMode.currentValue) {
        case true:
          this.map.removeControl(this.geocoder);
          this.buildDraw(this.map);
          break;
        case false:
          if (this.draw) {
            this.map.addControl(this.geocoder, 'top-left');
            this.map.removeControl(this.draw);
            this.map.removeControl(this.drawInfo);
          }
          break;
        default:
          break;
      }
    }

    if (triggerBuildMap && triggerBuildMap.currentValue === true) {
      navigator.geolocation.getCurrentPosition(
        (res) => {
          this.currCoor = [res.coords.longitude, res.coords.latitude];
          this.currentCoordinate.emit(this.currCoor);
          this.buildMap(...this.currCoor);
        },
        (err) => {
          console.log('error getting coordinate', err.message);
          this.buildMap();
        }
      );
    }
  }

  buildMap(lng: number = 0, lat: number = 0): void {
    this.map = new mapboxgl.Map({
      container: 'mapbox-container',
      style: this.mapStyle ?? environment.mapbox.defaultStyle,
      zoom: 18,
      center: [lng, lat],
    });
    this.map.addControl(new mapboxgl.NavigationControl());
    this.map.addControl(new CustomControl({}));
    this.geocoder = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl,
      placeholder: 'Area of Interest',
    });
    this.map.addControl(this.geocoder, 'top-left');
    this.map.on('load', (): void => {
      this.mapLoaded.emit(true);
      this.addCurrLocMarker();
      this.initArrowKeyNav(this.map);
      this.emitVisibleMapCentroid(this.map);
    });
    this.map.on('moveend', (): void => {
      this.emitVisibleMapCentroid(this.map);
    });
  }

  buildDraw(map: mapboxgl.Map): void {
    this.draw = new MapboxDraw({
      modes: { ...MapboxDraw.modes },
      userProperties: true,
      styles: [
        // default themes provided by MB Draw
        {
          id: 'gl-draw-polygon-fill-inactive',
          type: 'fill',
          filter: [
            'all',
            ['==', 'active', 'false'],
            ['==', '$type', 'Polygon'],
            ['!=', 'mode', 'static'],
          ],
          paint: {
            'fill-color': '#3bb2d0',
            'fill-outline-color': '#3bb2d0',
            'fill-opacity': 0.1,
          },
        },
        {
          id: 'gl-draw-polygon-fill-active',
          type: 'fill',
          filter: ['all', ['==', 'active', 'true'], ['==', '$type', 'Polygon']],
          paint: {
            'fill-color': '#fbb03b',
            'fill-outline-color': '#fbb03b',
            'fill-opacity': 0.1,
          },
        },
        {
          id: 'gl-draw-polygon-midpoint',
          type: 'circle',
          filter: ['all', ['==', '$type', 'Point'], ['==', 'meta', 'midpoint']],
          paint: {
            'circle-radius': 3,
            'circle-color': '#fbb03b',
          },
        },
        {
          id: 'gl-draw-polygon-stroke-inactive',
          type: 'line',
          filter: [
            'all',
            ['==', 'active', 'false'],
            ['==', '$type', 'Polygon'],
            ['!=', 'mode', 'static'],
          ],
          layout: {
            'line-cap': 'round',
            'line-join': 'round',
          },
          paint: {
            'line-color': '#3bb2d0',
            'line-width': 2,
          },
        },
        {
          id: 'gl-draw-polygon-stroke-active',
          type: 'line',
          filter: ['all', ['==', 'active', 'true'], ['==', '$type', 'Polygon']],
          layout: {
            'line-cap': 'round',
            'line-join': 'round',
          },
          paint: {
            'line-color': '#fbb03b',
            'line-dasharray': [0.2, 2],
            'line-width': 2,
          },
        },
        {
          id: 'gl-draw-line-inactive',
          type: 'line',
          filter: [
            'all',
            ['==', 'active', 'false'],
            ['==', '$type', 'LineString'],
            ['!=', 'mode', 'static'],
          ],
          layout: {
            'line-cap': 'round',
            'line-join': 'round',
          },
          paint: {
            'line-color': '#3bb2d0',
            'line-width': 2,
          },
        },
        {
          id: 'gl-draw-line-active',
          type: 'line',
          filter: [
            'all',
            ['==', '$type', 'LineString'],
            ['==', 'active', 'true'],
          ],
          layout: {
            'line-cap': 'round',
            'line-join': 'round',
          },
          paint: {
            'line-color': '#fbb03b',
            'line-width': 2,
          },
        },
        {
          id: 'gl-draw-polygon-and-line-vertex-stroke-inactive',
          type: 'circle',
          filter: [
            'all',
            ['==', 'meta', 'vertex'],
            ['==', '$type', 'Point'],
            ['!=', 'mode', 'static'],
          ],
          paint: {
            'circle-radius': 5,
            'circle-color': '#fff',
          },
        },
        {
          id: 'gl-draw-polygon-and-line-vertex-inactive',
          type: 'circle',
          filter: [
            'all',
            ['==', 'meta', 'vertex'],
            ['==', '$type', 'Point'],
            ['!=', 'mode', 'static'],
          ],
          paint: {
            'circle-radius': 3,
            'circle-color': '#fbb03b',
          },
        },
        {
          id: 'gl-draw-point-point-stroke-inactive',
          type: 'circle',
          filter: [
            'all',
            ['==', 'active', 'false'],
            ['==', '$type', 'Point'],
            ['==', 'meta', 'feature'],
            ['!=', 'mode', 'static'],
          ],
          paint: {
            'circle-radius': 5,
            'circle-opacity': 1,
            'circle-color': '#fff',
          },
        },
        {
          id: 'gl-draw-point-inactive',
          type: 'circle',
          filter: [
            'all',
            ['==', 'active', 'false'],
            ['==', '$type', 'Point'],
            ['==', 'meta', 'feature'],
            ['!=', 'mode', 'static'],
          ],
          paint: {
            'circle-radius': 3,
            'circle-color': '#3bb2d0',
          },
        },
        {
          id: 'gl-draw-point-stroke-active',
          type: 'circle',
          filter: [
            'all',
            ['==', '$type', 'Point'],
            ['==', 'active', 'true'],
            ['!=', 'meta', 'midpoint'],
          ],
          paint: {
            'circle-radius': 7,
            'circle-color': '#fff',
          },
        },
        {
          id: 'gl-draw-point-active',
          type: 'circle',
          filter: [
            'all',
            ['==', '$type', 'Point'],
            ['!=', 'meta', 'midpoint'],
            ['==', 'active', 'true'],
          ],
          paint: {
            'circle-radius': 5,
            'circle-color': '#fbb03b',
          },
        },
        {
          id: 'gl-draw-polygon-fill-static',
          type: 'fill',
          filter: ['all', ['==', 'mode', 'static'], ['==', '$type', 'Polygon']],
          paint: {
            'fill-color': '#404040',
            'fill-outline-color': '#404040',
            'fill-opacity': 0.1,
          },
        },
        {
          id: 'gl-draw-polygon-stroke-static',
          type: 'line',
          filter: ['all', ['==', 'mode', 'static'], ['==', '$type', 'Polygon']],
          layout: {
            'line-cap': 'round',
            'line-join': 'round',
          },
          paint: {
            'line-color': '#404040',
            'line-width': 2,
          },
        },
        {
          id: 'gl-draw-line-static',
          type: 'line',
          filter: [
            'all',
            ['==', 'mode', 'static'],
            ['==', '$type', 'LineString'],
          ],
          layout: {
            'line-cap': 'round',
            'line-join': 'round',
          },
          paint: {
            'line-color': '#404040',
            'line-width': 2,
          },
        },
        {
          id: 'gl-draw-point-static',
          type: 'circle',
          filter: ['all', ['==', 'mode', 'static'], ['==', '$type', 'Point']],
          paint: {
            'circle-radius': 5,
            'circle-color': '#404040',
          },
        },

        // new styles for toggling colors
        {
          id: 'gl-draw-polygon-color-picker',
          type: 'fill',
          filter: [
            'all',
            ['==', '$type', 'Polygon'],
            ['has', 'user_portColor'],
          ],
          paint: {
            'fill-color': ['get', 'user_portColor'],
            'fill-outline-color': ['get', 'user_portColor'],
            'fill-opacity': 0.5,
          },
        },
        {
          id: 'gl-draw-line-color-picker',
          type: 'line',
          filter: [
            'all',
            ['==', '$type', 'LineString'],
            ['has', 'user_portColor'],
          ],
          paint: {
            'line-color': ['get', 'user_portColor'],
            'line-width': 2,
          },
        },
        {
          id: 'gl-draw-point-color-picker',
          type: 'circle',
          filter: ['all', ['==', '$type', 'Point'], ['has', 'user_portColor']],
          paint: {
            'circle-radius': 3,
            'circle-color': ['get', 'user_portColor'],
          },
        },
      ],
    });
    this.drawInfo = new DrawInfoHelper();
    map.addControl(this.draw, 'top-right');
    map.addControl(this.drawInfo, 'top-left');
    map.on('draw.create', (e) => this.onDrawUpdate(e));
    map.on('draw.update', (e) => this.onDrawUpdate(e));
    map.on('draw.delete', (e) => this.onDrawDelete(e));
    map.on('draw.modechange', (e) => this.onDrawModeChange(e));

    if (this.drawings?.length > 0) {
      this.drawings.forEach((drawing) => {
        this.draw.add(drawing);
      });
    }
  }

  onDrawUpdate($event): void {
    let geojson;
    const { features, target } = $event;
    const { radius } = this.extrusionOpt;

    if (
      features[0].geometry.type === 'LineString' ||
      features[0].geometry.type === 'Point'
    ) {
      geojson = features[0];
      geojson = { ...buffer(geojson.geometry, radius / 1000), id: geojson.id };
    } else {
      geojson = features[0];
    }
    this.drawUpdated.emit(features[0]);
    this.generateExtrusionLayer(target, geojson);
  }

  onDrawDelete($event): void {
    const { features, target } = $event;
    this.drawDeleted.emit(features[0]);
    if (target.getLayer(`extrude-layer-${features[0].id}`) !== undefined) {
      target.removeLayer(`extrude-layer-${features[0].id}`);
    }
    if (target.getSource(`extrude-source-${features[0].id}`) !== undefined) {
      target.removeSource(`extrude-source-${features[0].id}`);
    }
  }

  onDrawModeChange($event): void {
    let elem;
    let text: string;
    const allElem = document.getElementsByClassName('mapbox-gl-draw_ctrl-draw-btn');

    switch ($event.mode) {
      case 'draw_line_string':
        elem = document.getElementsByClassName('mapbox-gl-draw_line')[0];
        elem.classList.add('active');
        text = DRAW_LINE_STRING_TEXT;
        break;

      case 'draw_polygon':
        elem = document.getElementsByClassName('mapbox-gl-draw_polygon')[0];
        elem.classList.add('active');
        text = DRAW_POLYGON_TEXT;
        break;

      case 'draw_point':
        elem = document.getElementsByClassName('mapbox-gl-draw_point')[0];
        elem.classList.add('active');
        text = DRAW_POINT_TEXT;
        break;

      default:
        for (var i = 0; i < allElem.length; i++) {
          allElem[i].classList.remove('active');
        }
        text = this.drawings.length > 0 ? DRAW_MORE_TEXT : DRAW_START_TEXT;
        break;
    }

    const helperText = document.querySelector(
      '#drawInfoHelper > p'
    );
    helperText.innerHTML = text;
  }

  initArrowKeyNav(map: mapboxgl.Map): void {
    const deltaDistance = 100;
    const deltaDegrees = 25;

    const easing = (t) => {
      return t * (2 - t);
    };

    map.getCanvas().focus();

    map.getCanvas().addEventListener(
      'keydown',
      (e) => {
        e.preventDefault();
        if (e.which === 38) {
          // up
          map.panBy([0, -deltaDistance], {
            easing,
          });
        } else if (e.which === 40) {
          // down
          map.panBy([0, deltaDistance], {
            easing,
          });
        } else if (e.which === 37) {
          // left
          map.easeTo({
            bearing: map.getBearing() - deltaDegrees,
            easing,
          });
        } else if (e.which === 39) {
          // right
          map.easeTo({
            bearing: map.getBearing() + deltaDegrees,
            easing,
          });
        }
      },
      true
    );
  }

  addCurrLocMarker(): void {
    const map = this.map;
    const size = this.markerSize;

    if (map.hasImage('pulsing-dot')) {map.removeImage('pulsing-dot'); }

    map.addImage(
      'pulsing-dot',
      {
        width: size,
        height: size,
        data: new Uint8Array(size * size * 4),

        onAdd(): void {
          const canvas = document.createElement('canvas');
          canvas.width = this.width;
          canvas.height = this.height;
          this.context = canvas.getContext('2d');
        },

        render(): boolean {
          const duration = 2000;
          const t = (performance.now() % duration) / duration;

          const radius = (size / 2) * 0.3;
          const outerRadius = (size / 2) * 0.7 * t + radius;
          const context = this.context;

          context.clearRect(0, 0, this.width, this.height);
          context.beginPath();
          context.arc(
            this.width / 2,
            this.height / 2,
            outerRadius,
            0,
            Math.PI * 2
          );
          context.fillStyle = 'rgba(0, 123, 255,' + (1 - t) + ')';
          context.fill();

          context.beginPath();
          context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
          context.fillStyle = 'rgba(0, 123, 255, 1)';
          context.strokeStyle = 'white';
          context.lineWidth = 2 + 4 * (1 - t);
          context.fill();
          context.stroke();

          this.data = context.getImageData(0, 0, this.width, this.height).data;

          map.triggerRepaint();

          return true;
        },
      },
      { pixelRatio: 2 }
    );
    if (map.getLayer('current-location-marker-layer')) {map.removeLayer('current-location-marker-layer'); }
    if (map.getSource('current-location-marker-source')) {map.removeSource('current-location-marker-source'); }
    map.addSource('current-location-marker-source', {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: this.currCoor,
            },
          },
        ],
      },
    });
    map.addLayer({
      id: 'current-location-marker-layer',
      type: 'symbol',
      source: 'current-location-marker-source',
      layout: {
        'icon-image': 'pulsing-dot',
      },
    });
  }

  generateExtrusionLayer(target: mapboxgl.Map, geojson): void {
    const { color, height, baseHeight, opacity } = this.extrusionOpt;

    const layerPaint = {
      'fill-extrusion-color': color,
      'fill-extrusion-height': height,
      'fill-extrusion-base': baseHeight,
      'fill-extrusion-opacity': opacity,
    };

    if (target.getLayer(`extrude-layer-${geojson.id}`) !== undefined) {
      target.removeLayer(`extrude-layer-${geojson.id}`);
    }
    if (target.getSource(`extrude-source-${geojson.id}`) !== undefined) {
      target.removeSource(`extrude-source-${geojson.id}`);
    }

    target.addSource(`extrude-source-${geojson.id}`, {
      type: 'geojson',
      data: geojson,
    });
    target.addLayer({
      id: `extrude-layer-${geojson.id}`,
      type: 'fill-extrusion',
      source: `extrude-source-${geojson.id}`,
      paint: layerPaint,
    });
  }

  emitVisibleMapCentroid(map): void {
    const { lng, lat } = map.getCenter();
    this.visibleMapCentroid.emit([lng, lat]);
  }
}

export interface ExtrusionOption {
  color: string;
  height: number;
  baseHeight: number;
  opacity: string;
  radius: number;
}

class CustomControl {
  _bearing;
  _pitch;
  _minpitchzoom;
  _map;
  _btn3d;
  _btnCrosshairs;
  _container;

  constructor({ bearing = -20, pitch = 70, minpitchzoom = null }) {
    this._bearing = bearing;
    this._pitch = pitch;
    this._minpitchzoom = minpitchzoom;
  }

  onAdd(map): void {
    this._map = map;
    const _this = this;

    this._btn3d = document.createElement('button');
    this._btn3d.className = 'mapboxgl-ctrl-icon mapboxgl-ctrl-pitchtoggle-3d';
    this._btn3d.type = 'button';
    this._btn3d['title'] = 'Toggle Pitch';
    this._btn3d['aria-label'] = 'Toggle Pitch';
    this._btn3d.onclick = () => {
      if (map.getPitch() === 0) {
        const options = { pitch: _this._pitch, bearing: _this._bearing };
        if (_this._minpitchzoom && map.getZoom() > _this._minpitchzoom) {
          options['zoom'] = _this._minpitchzoom;
        }
        map.easeTo(options);
        _this._btn3d.className =
          'mapboxgl-ctrl-icon mapboxgl-ctrl-pitchtoggle-2d';
      } else {
        map.easeTo({ pitch: 0, bearing: 0 });
        _this._btn3d.className =
          'mapboxgl-ctrl-icon mapboxgl-ctrl-pitchtoggle-3d';
      }
    };

    this._btnCrosshairs = document.createElement('button');
    this._btnCrosshairs.className =
      'mapboxgl-ctrl-icon mapboxgl-ctrl-find-my-location';
    this._btnCrosshairs.type = 'button';
    this._btnCrosshairs['title'] = 'Find my location';
    this._btnCrosshairs['aria-label'] = 'Find my location';
    this._btnCrosshairs.onclick = () => {
      const { geometry } = map.getSource(
        'current-location-marker-source'
      )._data.features[0];
      map.flyTo({
        center: geometry.coordinates,
      });
    };

    this._container = document.querySelector(
      '.mapboxgl-ctrl-group:first-child'
    );
    this._container.appendChild(this._btn3d);
    this._container.appendChild(this._btnCrosshairs);

    return this._container;
  }

  onRemove(): void {
    this._container.parentNode.removeChild(this._container);
    this._map = undefined;
  }
}

class DrawInfoHelper {
  _map;
  _content;
  _card;

  constructor() {}

  onAdd(map): void {
    this._map = map;

    this._card = document.createElement('div');
    this._card.id = 'drawInfoHelper';
    this._card.className = 'mapboxgl-ctrl card card-body';

    this._content = document.createElement('p');
    this._content.innerHTML = DRAW_START_TEXT;

    this._card.appendChild(this._content);

    return this._card;
  }

  onRemove(): void {
    this._card.parentNode.removeChild(this._card);
    this._map = undefined;
  }
}
