import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallWeatherWidgetComponent } from './small-weather-widget.component';

describe('SmallWeatherWidgetComponent', () => {
  let component: SmallWeatherWidgetComponent;
  let fixture: ComponentFixture<SmallWeatherWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmallWeatherWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallWeatherWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
