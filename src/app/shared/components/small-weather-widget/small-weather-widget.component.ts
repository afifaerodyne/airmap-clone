import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-small-weather-widget',
  templateUrl: './small-weather-widget.component.html',
  styleUrls: ['./small-weather-widget.component.sass'],
})
export class SmallWeatherWidgetComponent implements OnInit, OnChanges {
  @Input() lat = 0;
  @Input() lng = 0;
  currTemp = 0;
  weather = [];
  icon = 'fa-cloud';
  locationName: string;
  convertedLngLattoDMS: string;

  latitude: string;
  longitude: string;

  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.lat) {this.latitude = changes.lat.currentValue; }
    if (changes.lng) {this.longitude = changes.lng.currentValue; }

    this.getOpenWeatherAPI(this.longitude, this.latitude);
    this.getMapboxGeocoding(this.longitude, this.latitude);
    this.convertedLngLattoDMS = convertDMS(this.latitude, this.longitude);
  }

  getOpenWeatherAPI(longitude, latitude): void {
    this.httpClient
      .get(
        `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${environment.openWeather.apiKey}&units=metric`
      )
      .subscribe((res: OpenWeather) => {
        const { main, weather } = res;
        this.currTemp = main.temp;
        this.weather = weather;

        switch (weather[0].icon) {
          case '01d':
            this.icon = 'fa-sun';
            break;
          case '01n':
            this.icon = 'fa-moon';
            break;
          case '02d':
            this.icon = 'fa-cloud-sun';
            break;
          case '02n':
            this.icon = 'fa-cloud-moon';
            break;
          case '03d':
          case '03n':
            this.icon = 'fa-cloud';
            break;
          case '04d':
          case '04n':
            this.icon = 'fa-cloud';
            break;
          case '09d':
          case '09n':
            this.icon = 'fa-cloud-showers-heavy';
            break;
          case '10d':
            this.icon = 'fa-cloud-sun-rain';
            break;
          case '10n':
            this.icon = 'fa-cloud-moon-rain';
            break;
          case '11d':
          case '11n':
            this.icon = 'fa-bolt';
            break;
          case '13d':
          case '13n':
            this.icon = 'fa-snowflake';
            break;
          case '50d':
          case '50n':
            this.icon = 'fa-smog';
            break;
          default:
            this.icon = 'fa-cloud';
            break;
        }
      });
  }

  getMapboxGeocoding(longitude, latitude): void {
    this.httpClient
      .get(
        `https://api.mapbox.com/geocoding/v5/mapbox.places/${longitude},${latitude}.json?access_token=${environment.mapbox.accessToken}`
      )
      .subscribe((res) => {
        const unwantedPlaceType = ['poi', 'postcode'];
        const location = res['features'].filter(f => !unwantedPlaceType.includes(f.place_type[0]))[0];
        if (location) {
          this.locationName = location.text;
        } else {
          this.locationName = '-';
        }
      });
  }
}

export interface OpenWeather {
  coord: {
    lon: number;
    lat: number;
  };
  weather: [
    {
      id: number;
      main: string;
      description: string;
      icon: string;
    }
  ];
  base: string;
  main: {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
  };
  visibility: number;
  wind: {
    speed: number;
    deg: number;
  };
  clouds: {
    all: number;
  };
  dt: number;
  sys: {
    type: number;
    id: number;
    message: number;
    country: string;
    sunrise: number;
    sunset: number;
  };
  timezone: number;
  id: number;
  name: string;
  cod: number;
}

function toDegreesMinutesAndSeconds(coordinate): string {
  const absolute = Math.abs(coordinate);
  const degrees = Math.floor(absolute);
  const minutesNotTruncated = (absolute - degrees) * 60;
  const minutes = Math.floor(minutesNotTruncated);
  const seconds = Math.floor((minutesNotTruncated - minutes) * 60 * 100) / 100;

  return degrees + '° ' + minutes + '\' ' + seconds + '"';
}

function convertDMS(lat, lng): string {
  const latitude = toDegreesMinutesAndSeconds(lat);
  const latitudeCardinal = lat >= 0 ? 'N' : 'S';

  const longitude = toDegreesMinutesAndSeconds(lng);
  const longitudeCardinal = lng >= 0 ? 'E' : 'W';

  return (
    latitude +
    ' ' +
    latitudeCardinal +
    ', ' +
    longitude +
    ' ' +
    longitudeCardinal
  );
}
