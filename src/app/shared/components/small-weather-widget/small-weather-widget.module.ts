import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmallWeatherWidgetComponent } from './small-weather-widget.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [SmallWeatherWidgetComponent],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [SmallWeatherWidgetComponent]
})
export class SmallWeatherWidgetModule { }
