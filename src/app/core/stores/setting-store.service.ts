import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingStoreService {
  public setting$: BehaviorSubject<SettingInterface> = new BehaviorSubject({});

  constructor() { }
}

export interface SettingInterface {
  map_style?: string;
  flight_is_public?: boolean;
}
