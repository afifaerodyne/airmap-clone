import { TestBed } from '@angular/core/testing';

import { SettingStoreService } from './setting-store.service';

describe('SettingStoreService', () => {
  let service: SettingStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SettingStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
